# Changelog

## [1.0.0] - 2022-12-02

### Added

- Support for encrypting 64-bit plaintexts using 128-bit keys.
- Support for decrypting 64-bit ciphertexts using 128-bit keys.

### Changed

- DRYed up key and block validation.

## [0.2.0] - 2022-11-29

### Added

- Key and block length validation.

## [0.1.0] - 2022-11-26

### Added

- This gem!
- Support for encrypting 64-bit plaintexts using 80-bit keys.
- Support for decrypting 64-bit ciphertexts using 80-bit keys.
