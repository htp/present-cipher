# Present::Cipher

A Ruby implementation of [the PRESENT block cipher](https://www.iacr.org/archive/ches2007/47270450/47270450.pdf).

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add present-cipher

If bundler is not being used to manage dependencies, install the gem by executing:

    $ gem install present-cipher

## Usage

Present::Cipher encrypts 64-bit plaintexts and decrypts 64-bit ciphertexts using 80- or 128-bit keys.

### Generating a Key

Using `SecureRandom`:

```
require "present/cipher"
require "securerandom"

key = SecureRandom.bytes(Present::Cipher::KEY_BYTESIZE_80) # Make sure to save this value somewhere safe!
```

If you prefer, pass `Present::Cipher::KEY_BYTESIZE_128` to generate a 128-bit key.

### Encrypting

```
require "present/cipher"

cipher = Present::Cipher.new(key)
ciphertext = cipher.encrypt(plaintext)
```

### Decrypting

```
require "present/cipher"

cipher = Present::Cipher.new(key)
plaintext = cipher.decrypt(ciphertext)
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.

## Contributing

Bug reports and pull requests are welcome on Codeberg at https://codeberg.org/htp/present-cipher. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://codeberg.org/htp/present-cipher/blob/main/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Present::Cipher project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://codeberg.org/htp/present-cipher/blob/main/CODE_OF_CONDUCT.md).
