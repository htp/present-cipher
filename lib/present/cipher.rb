# frozen_string_literal: true

require_relative "cipher/error"
require_relative "cipher/version"

module Present
  class Cipher
    KEY_BITSIZE_80 = 80
    KEY_BITSIZE_128 = 128
    BLOCK_BITSIZE = 64

    KEY_BYTESIZE_80 = KEY_BITSIZE_80 / 8
    KEY_BYTESIZE_128 = KEY_BITSIZE_128 / 8
    BLOCK_BYTESIZE = BLOCK_BITSIZE / 8

    S_BOX = [
      0x0C, 0x05, 0x06, 0x0b, 0x09, 0x00, 0x0a, 0x0d, 0x03, 0x0e, 0x0f, 0x08, 0x04, 0x07, 0x01, 0x02,
    ]

    INVERSE_S_BOX = 0.upto(S_BOX.length - 1).map { |n| S_BOX.index(n) }

    P_BOX = [
      0x00, 0x10, 0x20, 0x30, 0x01, 0x11, 0x21, 0x31, 0x02, 0x12, 0x22, 0x32, 0x03, 0x13, 0x23, 0x33,
      0x04, 0x14, 0x24, 0x34, 0x05, 0x15, 0x25, 0x35, 0x06, 0x16, 0x26, 0x36, 0x07, 0x17, 0x27, 0x37,
      0x08, 0x18, 0x28, 0x38, 0x09, 0x19, 0x29, 0x39, 0x0a, 0x1a, 0x2a, 0x3a, 0x0b, 0x1b, 0x2b, 0x3b,
      0x0c, 0x1c, 0x2c, 0x3c, 0x0d, 0x1d, 0x2d, 0x3d, 0x0e, 0x1e, 0x2e, 0x3e, 0x0f, 0x1f, 0x2f, 0x3f,
    ]

    INVERSE_P_BOX = 0.upto(P_BOX.length - 1).map { |n| P_BOX.index(n) }

    def initialize(key)
      validate(key, as: :key)

      @key = key.dup
    end

    def encrypt(bytes)
      validate(bytes, as: :block)

      bits = bytes.unpack1("Q>")

      0.upto(30) do |i|
        bits = apply_round_key(bits, round_keys[i])
        bits = apply_substitution_box(bits, S_BOX)
        bits = apply_permutation_box(bits, P_BOX)
      end

      bits = apply_round_key(bits, round_keys[31])

      [bits].pack("Q>")
    end

    def decrypt(bytes)
      validate(bytes, as: :block)

      bits = bytes.unpack1("Q>")

      31.downto(1) do |i|
        bits = apply_round_key(bits, round_keys[i])
        bits = apply_permutation_box(bits, INVERSE_P_BOX)
        bits = apply_substitution_box(bits, INVERSE_S_BOX)
      end

      bits = apply_round_key(bits, round_keys[0])

      [bits].pack("Q>")
    end

    private

    def validate(bytes, parameters)
      bitsize = bytes.bytesize * 8
      aspect = parameters[:as]

      case aspect
      when :key
        klass = KeyError
        bitsizes = [KEY_BITSIZE_80, KEY_BITSIZE_128]
      when :block
        klass = BlockError
        bitsizes = [BLOCK_BITSIZE]
      else
        raise ArgumentError, "aspect passed via :as must be one of :key or :block"
      end

      raise klass, "#{aspect} length is invalid (expected #{bitsizes.join(" or ")} bits; got #{bitsize})" unless bitsizes.include?(bitsize)
    end

    def round_keys
      @round_keys ||= begin
        case @key.bytesize
        when KEY_BYTESIZE_80
          round_keys_80(@key)
        when KEY_BYTESIZE_128
          round_keys_128(@key)
        else
          raise NotSupportedError
        end
      end
    end

    def round_keys_80(key)
      round_keys = []

      register = key.unpack1("B80").to_i(2)

      1.upto(32) do |i|
        round_keys << (register >> 16)

        # Rotate 61 bits to the left.
        register = ((register & ((1 << 19) - 1)) << 61) | (register >> 19)

        # Push the leftmost four bits through the substitution box.
        register = (S_BOX[(register >> 76)] << 76) | (register & ((1 << 76) - 1))

        # XOR the round counter into registers 19 through 15.
        register = ((register >> 20) << 20) | ((((register >> 15) & ((1 << 5) - 1)) ^ i) << 15) | (register & ((1 << 15) - 1))
      end

      round_keys
    end

    def round_keys_128(key)
      round_keys = []

      register = key.unpack1("B128").to_i(2)

      1.upto(32) do |i|
        round_keys << (register >> 64)

        # Rotate 61 bits to the left.
        register = ((register & ((1 << 67) - 1)) << 61) | (register >> 67)

        # Push the leftmost four bits through the substitution box.
        register = (S_BOX[(register >> 124)] << 124) | (register & ((1 << 124) - 1))

        # Push the next leftmost four bits through the substitution box.
        register = ((register >> 124) << 124) | (S_BOX[(register >> 120) & ((1 << 4) - 1)] << 120) | (register & ((1 << 120) - 1))

        # XOR the round counter into registers 66 through 62.
        register = ((register >> 67) << 67) | ((((register >> 62) & ((1 << 5) - 1)) ^ i) << 62) | (register & ((1 << 62) - 1))
      end

      round_keys
    end

    def apply_round_key(bits, key)
      bits ^ key
    end

    def apply_substitution_box(bits, box)
      substituted_bits = 0

      0.upto(box.length - 1) do |i|
        substituted_bits |= box[((bits >> (i * 4)) & ((1 << 4) - 1))] << (i * 4)
      end

      substituted_bits
    end

    def apply_permutation_box(bits, box)
      permuted_bits = 0

      0.upto(box.length - 1) do |i|
        permuted_bits |= ((bits >> i) & 1) << box[i]
      end

      permuted_bits
    end
  end
end
