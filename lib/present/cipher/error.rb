module Present
  class Cipher
    class Error < StandardError
    end

    class NotSupportedError < Error
    end

    class KeyError < Error
    end

    class BlockError < Error
    end
  end
end
