# frozen_string_literal: true

require "test_helper"

describe Present::Cipher do
  before do
    # https://www.iacr.org/archive/ches2007/47270450/47270450.pdf
    @fixtures_80 = [
      ["\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00".b, "\x00\x00\x00\x00\x00\x00\x00\x00".b, "\x55\x79\xc1\x38\x7b\x22\x84\x45".b],
      ["\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00".b, "\xff\xff\xff\xff\xff\xff\xff\xff".b, "\xa1\x12\xff\xc7\x2f\x68\x41\x7b".b],
      ["\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff".b, "\x00\x00\x00\x00\x00\x00\x00\x00".b, "\xe7\x2c\x46\xc0\xf5\x94\x50\x49".b],
      ["\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff".b, "\xff\xff\xff\xff\xff\xff\xff\xff".b, "\x33\x33\xdc\xd3\x21\x32\x10\xd2".b],
    ]

    # https://crypto.stackexchange.com/questions/70906/where-can-i-find-test-vectors-for-the-present-cipher-with-a-128-bit-key
    @fixtures_128 = [
      ["\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00".b, "\x00\x00\x00\x00\x00\x00\x00\x00".b, "\x96\xdb\x70\x2a\x2e\x69\x00\xaf".b],
      ["\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00".b, "\xff\xff\xff\xff\xff\xff\xff\xff".b, "\x3c\x60\x19\xe5\xe5\xed\xd5\x63".b],
      ["\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff".b, "\x00\x00\x00\x00\x00\x00\x00\x00".b, "\x13\x23\x8c\x71\x02\x72\xa5\xd8".b],
      ["\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff".b, "\xff\xff\xff\xff\xff\xff\xff\xff".b, "\x62\x8d\x9f\xbd\x42\x18\xe5\xb4".b],
    ]
  end

  describe "#initialize" do
    it "validates the key's length" do
      [@fixtures_80.first, @fixtures_128.first].each do |key, _, _|
        [-1, 1].each do |delta|
          bytesize = key.bytesize + delta
          bitsize = bytesize * 8

          error = assert_raises(Present::Cipher::KeyError) do
            Present::Cipher.new((key * 2).slice(0, bytesize))
          end

          assert_equal error.message, "key length is invalid (expected 80 or 128 bits; got #{bitsize})"
        end
      end
    end
  end

  describe "#encrypt" do
    it "validates the plaintext's length" do
      [@fixtures_80.first, @fixtures_128.first].each do |key, plaintext, _|
        [-1, 1].each do |delta|
          bytesize = plaintext.bytesize + delta
          bitsize = bytesize * 8

          cipher = Present::Cipher.new(key)

          error = assert_raises(Present::Cipher::BlockError) do
            cipher.encrypt((plaintext * 2).slice(0, bytesize))
          end

          assert_equal error.message, "block length is invalid (expected 64 bits; got #{bitsize})"
        end
      end
    end

    it "encrypts" do
      (@fixtures_80 + @fixtures_128).each do |key, plaintext, ciphertext|
        cipher = Present::Cipher.new(key)
        result = cipher.encrypt(plaintext)

        assert_equal ciphertext, result
      end
    end
  end

  describe "#decrypt" do
    it "validates the ciphertext's length" do
      [@fixtures_80.first, @fixtures_128.first].each do |key, _, ciphertext|
        [-1, 1].each do |delta|
          bytesize = ciphertext.bytesize + delta
          bitsize = bytesize * 8

          cipher = Present::Cipher.new(key)

          error = assert_raises(Present::Cipher::BlockError) do
            cipher.decrypt((ciphertext * 2).slice(0, bytesize))
          end

          assert_equal error.message, "block length is invalid (expected 64 bits; got #{bitsize})"
        end
      end
    end

    it "decrypts" do
      (@fixtures_80 + @fixtures_128).each do |key, plaintext, ciphertext|
        cipher = Present::Cipher.new(key)
        result = cipher.decrypt(ciphertext)

        assert_equal plaintext, result
      end
    end
  end
end
