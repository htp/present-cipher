# frozen_string_literal: true

require "test_helper"

describe Present::Cipher do
  describe "version" do
    it "isn't nil" do
      refute_nil ::Present::Cipher::VERSION
    end
  end
end
